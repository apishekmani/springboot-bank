package com.example.demo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class  Account {
    @Column(name="password")
    private String password;
    @Column(name="AccountHolderName")
    private String AccountHolderName;
    @Column(name="firstName")
    private String firstName;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private long id;
    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="Address")
    private Address address;
}
