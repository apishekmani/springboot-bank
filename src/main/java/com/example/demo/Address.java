package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="address")
public class Address {
    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    private long id;
    private String street;
    private String post;
    private String taluk;
    private String district;
    private String state;
    private String country;
    private long pincode;

    public Address(String street,String post, String taluk, String district, String state, String country, long pincode) {
        this.street=street;
        this.post=post;
        this.taluk=taluk;
        this.district=district;
        this.state=state;
        this.country=country;
        this.pincode=pincode;
    }
}