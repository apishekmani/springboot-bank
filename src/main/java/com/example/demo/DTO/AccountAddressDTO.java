package com.example.demo.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountAddressDTO {

    private String password;
    private String AccountHolderName;
    private String firstName;
    private String street;
    private String post;
    private String taluk;
    private String district;
    private String state;
    private String country;
    private long pincode;
}
