package com.example.demo.controller;

import com.example.demo.DTO.AccountAddressDTO;
import com.example.demo.Repository.AccountRepository;
import com.example.demo.service.AccountService;
import com.example.demo.service.AccountServiceImplementation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AccountController{
    @Autowired
    private AccountServiceImplementation accountServiceImplementation;
    @Autowired
    private AccountRepository accountRepository;

    @RequestMapping("/account")
    public ResponseEntity<List<AccountAddressDTO>> get(){
        //accountServiceImplementation.get();
        return (accountServiceImplementation.get());
    }

    @PostMapping(value="/accountCreation")
    public ResponseEntity<HttpStatus>addAccount(@RequestBody AccountAddressDTO accountAddressDTO){
        accountServiceImplementation.addAccount(accountAddressDTO);
        return ResponseEntity.accepted().body(HttpStatus.CREATED);
    }

    @PutMapping(value="/accountUpdate/{id}")
    public ResponseEntity<HttpStatus>updateAccount(@RequestBody AccountAddressDTO accountAddressDTO,@PathVariable long id){
        accountServiceImplementation.updateAccount(accountAddressDTO,id);
        return ResponseEntity.accepted().body(HttpStatus.ACCEPTED);
    }

    @DeleteMapping(value="/accountDelete/{id}")
    public ResponseEntity<HttpStatus>deleteAccount(@PathVariable long id){
        accountServiceImplementation.deleteAccount(id);
        return ResponseEntity.ok().body(HttpStatus.OK);
    }

    @RequestMapping("/accountwithpagination/{offset}/{pageSize}/{field}")
    public ResponseEntity<List<AccountAddressDTO>> withPagination(@PathVariable  int offset,@PathVariable int pageSize,@PathVariable String field){
        return (accountServiceImplementation.withPagination(offset,pageSize,field));
    }

}
