package com.example.demo.service;

import com.example.demo.Account;
import com.example.demo.DTO.AccountAddressDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AccountService {
    public ResponseEntity<HttpStatus>addAccount(AccountAddressDTO accountAddressDTO);
    public ResponseEntity<List<AccountAddressDTO>>get();
    public ResponseEntity<HttpStatus>updateAccount(AccountAddressDTO accountAddressDTO,long id);
    public ResponseEntity<HttpStatus>deleteAccount(long id);
    }
