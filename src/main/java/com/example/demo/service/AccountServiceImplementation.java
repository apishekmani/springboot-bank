package com.example.demo.service;
import com.example.demo.Account;
import com.example.demo.Address;
import com.example.demo.DTO.AccountAddressDTO;
import com.example.demo.Repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;


@Service
public class AccountServiceImplementation {

    @Autowired
    private AccountRepository accountRepository;
    public ResponseEntity<HttpStatus> addAccount(AccountAddressDTO accountAddressDTO) {
        Account account =new Account();
        accountRepository.save(convertEntityToAccount(account,accountAddressDTO));
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
    public ResponseEntity<List<AccountAddressDTO>>get() {
        List<AccountAddressDTO>accountAddressDTOList=accountRepository.findAll().stream()
                .map(account ->convertEntityToDto(account)).collect(Collectors.toList());
       return new ResponseEntity<>(accountAddressDTOList,HttpStatus.OK);
    }

    private AccountAddressDTO convertEntityToDto(Account account){
        AccountAddressDTO accountAddressDTO=new AccountAddressDTO();
        accountAddressDTO.setPassword(account.getPassword());
        accountAddressDTO.setAccountHolderName(account.getAccountHolderName());
        accountAddressDTO.setFirstName(account.getFirstName());
        accountAddressDTO.setStreet(account.getAddress().getStreet());
        accountAddressDTO.setPost(account.getAddress().getPost());
        accountAddressDTO.setTaluk(account.getAddress().getTaluk());
        accountAddressDTO.setDistrict(account.getAddress().getDistrict());
        accountAddressDTO.setState(account.getAddress().getState());
        accountAddressDTO.setCountry(account.getAddress().getCountry());
        accountAddressDTO.setPincode(account.getAddress().getPincode());
        return accountAddressDTO;
    }
    private Account convertEntityToAccount(Account account,AccountAddressDTO accountAddressDTO){
        account.setPassword(accountAddressDTO.getPassword());
        account.setAccountHolderName(accountAddressDTO.getAccountHolderName());
        account.setFirstName(accountAddressDTO.getFirstName());
        account.setAddress(new Address(accountAddressDTO.getStreet(),accountAddressDTO.getPost(),accountAddressDTO.getTaluk(),
                accountAddressDTO.getDistrict(),accountAddressDTO.getState(),accountAddressDTO.getCountry(),
                accountAddressDTO.getPincode()));
        return account;
    }

    public ResponseEntity<HttpStatus>updateAccount(AccountAddressDTO accountAddressDTO, long id) {
        Account accountUpdate=accountRepository.findById(id).orElseThrow(() -> new RuntimeException());;
        accountRepository.save(convertEntityToAccount(accountUpdate,accountAddressDTO));
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    public ResponseEntity<HttpStatus>deleteAccount(long id) {
        Account accountDelete=accountRepository.findById(id).orElseThrow(()-> new RuntimeException());
        accountRepository.delete(accountDelete);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<List<AccountAddressDTO>> withPagination(int offset,int pageSize,String field) {
        List<AccountAddressDTO>accountAddressDTOList=accountRepository.findAll(PageRequest.of(offset,pageSize).withSort(Sort.by(field))).stream()
                .map(account ->convertEntityToDto(account)).collect(Collectors.toList());
        return  new ResponseEntity<>(accountAddressDTOList,HttpStatus.OK);
    }
}